%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.05.2021
%%% @doc

%% ====================================================================
%% Types
%% ====================================================================
                         
%% ====================================================================
%% Constants and defaults
%% ====================================================================

-define(EtsNames, reglib_globalnames).

%% ====================================================================
%% Define modules
%% ====================================================================

-define(BASICLIB, basiclib).

-define(APP, reglib).
-define(RoleName, "reglib").

-define(SUPV, reglib_supv).
-define(CFG, reglib_config).
-define(STORE, reglib_store).

-define(CLI, reglib_client).
-define(SRV, reglib_server).

-define(STATE, reglib_state_srv).
-define(NAME, reglib_name_srv).
-define(PING, reglib_ping).

%% ------
%% From basiclib
%% ------
-define(BU, basiclib_utils).
-define(BLmulticall, basiclib_multicall).
-define(BLstore, basiclib_store).
-define(BLstore_template, basiclib_store_srv_template).
-define(BLping, basiclib_ping).
-define(BLmonitor, basiclib_monitor_srv).
-define(BLlog, basiclib_log).
-define(BLrpc, basiclib_rpc).

%% ====================================================================
%% Define logs
%% ====================================================================

-define(LOGFILE, {env,?APP}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level, ?CFG:log_destination(?LOGFILE), Text)).

-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), {Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(Level, ?CFG:log_destination(?LOGFILE), Text)).
                             
