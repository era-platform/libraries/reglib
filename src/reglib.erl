%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.05.2021
%%% @doc reglib application (registrar of global names).
%%%      Store data in local ets only.
%%%      Sync data to other replicas.
%%%      Name::term() -> Pid::pid(), Name::term() -> [Pid::pid()]
%%%   Application could be setup by application:set_env:
%%%      log_destination
%%%          descriptor to define log file folder and prefix.
%%%          Default: {'env','reg'}
%%%      get_server_nodes_function()
%%%             :: function() -> {ok,[Node::node()]}.
%%%          Called to get all linked replicated server nodes (to forward data, sync data, define itself).
%%%          Default: {ok,[]}. Means no replication, only local mode (only registers current node's prodesses).
%%%      get_sync_nodes_function()
%%%             :: function() -> {ok,[Node:node()]}.
%%%          Used only by reg servers (node itself contains in server_nodes).
%%%          Called to get all linked nodes in area of cluster, where current regserver is used. (ping periodically to force connect and sync)
%%%          Default: {ok,[]}. Means no sync
%%%      notify_function()
%%%             :: function(Name,Pid,Status::down | up) -> ok.
%%%          Used only by reg servers (node itself contains in server_nodes).
%%%          Called to notify external service on Process reg/down.
%%%          Default: ok
%%%      ping_timeout
%%%          Used as interval between force ping other server nodes
%%%          If 0 then do not use ping (only at restart)
%%%          Default: 1000
%%%      ping_clients_period_sec
%%%          Set how often should server check-ping all clients (consider disconnect and inform other nodes)
%%%          If 0 then do not use check-ping clients.
%%%          Default: 10
%%%      ping_clients_attempts_before_disconnect
%%%          When ping_clients_period_sec > 0 then count failed times for every node, when threshold then do disconnect
%%%          0 = 1 - disconnect after first ping failed
%%%          Default: 1
%%% TODO: log file setup from opts.
%%% TODO: ping from clients to server periodically instead of back. It's more usual way.
%%% TODO: cache requested pid on client, monitor it and auto remove by DOWN. It could not be used if alive process could be unregistered.
%%% TODO: register/unregister/reload callback (to handle outside and put in dms)

-module(reglib).
-author('Peter <tbotc@yandex.ru>').

-behaviour(application).

% client facade
-export([registered_pids/0,
         registered_names_local_size/0,
         registered_names_local/0,
         registered_names/0,
         register_name/2,
         unregister_name/1,
         unregister_name_globally/2,
         unregister_name_locally/2,
         whereis_name/1,
         whereis_name_all/1,
         get_nodes/1,
         send/2]).

% global_name server
-export([resync_global_names/1]).

% application
-export([start/0,
         stop/0]).

% application
-export([start/2,
         stop/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-type name() :: binary() | atom().

%% ====================================================================
%% Public
%% ====================================================================

%% -----------------------------------------------------
%% LOADER FACADE
%% -----------------------------------------------------

%% -------------------------------------
%% Starts application
%% -------------------------------------
start() ->
    case application:ensure_all_started(?APP, permanent) of
        {ok, _Started} -> ok;
        Error -> Error
    end.

%% -------------------------------------
%% Stops application
%% -------------------------------------
stop() -> application:stop(?APP).

%% -----------------------------------------------------
%% CLIENT FACADE
%% -----------------------------------------------------

%% -------------------------------------
-spec registered_pids() -> [{Name :: name(), Pid :: pid()}].
%% -------------------------------------
registered_pids() -> ?CLI:registered_pids().

%% -------------------------------------
-spec registered_names_local_size() -> integer().
%% -------------------------------------
registered_names_local_size() -> ?CLI:registered_names_local_size().

%% -------------------------------------
-spec registered_names_local() -> [Name :: name()].
%% -------------------------------------
registered_names_local() -> ?CLI:registered_names_local().

%% -------------------------------------
-spec registered_names() -> [Name :: name()].
%% -------------------------------------
registered_names() -> ?CLI:registered_names().

%% -------------------------------------
-spec register_name(Name :: name(), Pid :: pid()) -> yes.
%% -------------------------------------
register_name(Name,Pid) -> ?CLI:register_name(Name,Pid).

%% -------------------------------------
%% Unregister pid (or pids), that is local on current node. Forward request to regservers.
%% -------------------------------------
-spec unregister_name(Name :: name()) -> ok.
%% -------------------------------------
unregister_name(Name) -> ?CLI:unregister_name(Name).

%% -------------------------------------
%% Unregister some pids: local or all, and forward request to regservers.
%% -------------------------------------
-spec unregister_name_globally(Name :: name(), local | all) -> ok.
%% -------------------------------------
unregister_name_globally(Name, Mode) when Mode==local; Mode==all -> ?CLI:unregister_name_globally(Name, Mode).

%% -------------------------------------
%% Unregister some pids LOCALLY: all or directly selected. DOES NOT FORWARD REQUEST TO REGSERVERS.
%% -------------------------------------
-spec unregister_name_locally(Name :: name(), all | [pid()]) -> ok.
%% -------------------------------------
unregister_name_locally(Name, all) -> ?CLI:unregister_name_locally(Name, {srv,all});
unregister_name_locally(Name, PidsToDel) -> ?CLI:unregister_name_locally(Name, {srv,PidsToDel}).

%% -------------------------------------
-spec whereis_name(Name :: name()) -> pid() | undefined.
%% -------------------------------------
whereis_name(Name) -> ?CLI:whereis_name(Name).

%% -------------------------------------
-spec whereis_name_all(Name :: name()) -> [pid()].
%% -------------------------------------
whereis_name_all(Name) -> ?CLI:whereis_name_all(Name).

%% -------------------------------------
-spec get_nodes(Name :: name()) -> [node()].
%% -------------------------------------
get_nodes(Name) -> ?CLI:get_nodes(Name).

%% -------------------------------------
-spec send(Name :: name(), Msg :: term()) -> pid() | {badarg, {Name :: name(), Msg :: term()}}.
%% -------------------------------------
send(Name,Msg) -> ?CLI:send(Name,Msg).

%% -----------------------------------------------------
%% SERVER FACADE
%% -----------------------------------------------------

%% -------------------------------------
-spec resync_global_names(Nodes :: [node()]) -> ok.
%% -------------------------------------
resync_global_names(Nodes) -> ?NAME:resync(Nodes).

%% ===================================================================
%% Application callback functions
%% ===================================================================

%% -------------------------------------
%% @doc Starts application
%% -------------------------------------
start(_, _) ->
    setup_start_ts(),
    setup_dependencies(),
    ensure_deps_started(),
    ?OUT('$info',"~ts. ~p start", [?RoleName, self()]),
    ?SUPV:start_link(undefined).

%% -------------------------------------
%% @doc Stops application.
%% -------------------------------------
stop(State) ->
    ?OUT('$info',"~ts: Application stopped (~120p)", [?RoleName, State]),
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% --------------------------------------
%% @private
%% setup start_utc to applicaiton's env for monitor purposes
%% --------------------------------------
setup_start_ts() ->
    application:set_env(?APP,start_utc,?BU:timestamp()).

%% --------------------------------------
%% @private
%% setup opts of dependency apps
%% --------------------------------------
setup_dependencies() -> ok.

%% --------------------------------------
%% @private
%% starts deps applications, that are not linked to app, and to ensure
%% --------------------------------------
ensure_deps_started() ->
    {ok,_} = application:ensure_all_started(?BASICLIB, permanent).