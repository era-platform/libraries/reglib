%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.05.2021
%%% @doc General server that stores and monitors registered pids.
%%%         Used both on client and server nodes. But in different sync ways.
%%%       Server:
%%%       Client^

-module(reglib_name_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([register_name/2,
         fwd_register_name/2,
         cache_register_name/2,
         unregister_name/1,
         fwd_unregister_name/2,
         unregister_name_globally/2,
         unregister_name_locally/2,
         whereis_name/1,
         whereis_name_all/1,
         get_nodes/1]).

-export([get_size/0,
         get_data/0,
         get_data_sync/0,
         registered_names/0,
         resync/1,
         get_data_checksum/0,
         sync_heartbeat/2]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(state, {
    mode = client :: server | client, % if current node is reg server
    regserver_nodes = [], % reg server nodes
    sitenodes = [], % local site nodes
    ets_names, % ets of registered names ({Name::term() => Pids::list()})
    ets_pids, % ets of monitor references ({Pid::pid() => #val{pid,monref,name,ts}})
    lastupts, % timestamp of last data change
    syncs = [], % list of sync monitors
    %
    cfgtimerref, % reference of configuration timer
    cfgref, % reference to check configuration timer message
    % server timer
    pingtimerref, % reference of ping timer
    pingref, % reference to check ping timer message
    % permanent timer
    heartbeat_ref, % reference to check heartbeat command message
    heartbeat_timerref, % reference of heartbeat timer
    heartbeat_count = 0, % heartbeat counter
    pingclients_period_sec = 0, % period to check client nodes
    pingclients_attempts_before_disconnect = 1, % how many attempts of ping before ping+disconnect
    pingclients_ref, % reference of ping all procedure
    pingclients_results = #{}, % #{node => Count :: integer()} pang/timeout times
    ext = #{}
  }).

-record(val, {
    name,
    pid,
    monref,
    ts
  }).

-define(TIMEOUT_CFG,10000).
-define(InitialPingTimeout,1000).
-define(HeartbeatTimeout,?InitialPingTimeout+100).

-define(RESYNC(Cnt), Cnt rem 300==0).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, Opts, []).

%% ---------------------------------
%% register name to pid
%% ---------------------------------
register_name(Name,Pid) ->
    gen_server:call(?MODULE,{register_name,'client',Name,Pid}).

%% should not be forwarded to reg servers
fwd_register_name(Name,Pid) ->
    gen_server:call(?MODULE,{register_name,'server',Name,Pid}).

%% should not be forwarded to reg servers, locally cached some external pid
cache_register_name(Name,Pid) ->
    gen_server:call(?MODULE,{register_name,'cache',Name,Pid}).

%% ---------------------------------
%% unregister name (only local pids, registered by current node, should be forked to reg servers)
%% ---------------------------------
unregister_name(Name) ->
    gen_server:call(?MODULE,{unregister_name,Name,local}).

%% should not be forwarded to reg servers
fwd_unregister_name(Name,{srv,ModeArg}) when ModeArg==all; is_list(ModeArg) ->
    unregister_name_locally(Name,{srv,ModeArg}).

%% ---------------------------------
%% unregister name (call to server, should not be forwarded to reg servers)
%% ---------------------------------
unregister_name_globally(Name,Mode) when Mode==local; Mode==all ->
    gen_server:call(?MODULE,{unregister_name,Name,Mode}).

%% ---------------------------------
%% unregister name (call to server, should not be forwarded to reg servers)
%% ---------------------------------
unregister_name_locally(Name,{srv,ModeArg}) when ModeArg==all; is_list(ModeArg) ->
    gen_server:call(?MODULE,{unregister_name,Name,{srv,ModeArg}}).

%% ---------------------------------
%% return pid of registered name or undefined
%% ---------------------------------
whereis_name(Name) ->
    case catch ets:lookup(?EtsNames,Name) of
        [{_,[Pid]}] -> Pid;
        [{_,[_|_]=Pids}] -> select_pid(Name,Pids);
        _ -> undefined
    end.

%% ---------------------------------
%% return all pids of registered name or undefined
%% ---------------------------------
whereis_name_all(Name) ->
    case catch ets:lookup(?EtsNames,Name) of
        [{_,[Pid]}] -> [Pid];
        [{_,[_|_]=Pids}] -> Pids;
        _ -> []
    end.

%% ---------------------------------
%% return all pids of registered name or undefined
%% ---------------------------------
get_nodes(Name) ->
    case catch ets:lookup(?EtsNames,Name) of
        [{_,[Pid]}] -> [node(Pid)];
        [{_,[_|_]=Pids}] -> [node(Pid) || Pid <- Pids];
        _ -> []
    end.

%% ------------------------------------------------------------------

%% ---------------------------------
%% return size of registered names storage
%% ---------------------------------
get_size() ->
    case catch ets:info(?EtsNames,size) of
        N when is_integer(N) -> N;
        _ -> 0
    end.

%% ---------------------------------
%% return all registered names & pids
%% ---------------------------------
get_data() -> get_data(fun(_) -> [] end).

%% @private
get_data(Fdef) when is_function(Fdef,1) ->
    case catch ets:tab2list(?EtsNames) of
        List when is_list(List) -> List;
        T -> Fdef(T)
    end.

%% ---------------------------------
%% return all registered names & pids in sync mode
%% ---------------------------------
get_data_sync() -> get_data_sync(3).
get_data_sync(0) -> get_data(fun(_) -> error end);
get_data_sync(SyncAttempts) ->
    case whereis(?MODULE) of
        Pid when is_pid(Pid) ->
            case catch gen_server:call(?MODULE,{get_data_sync},2000) of
                List when is_list(List) -> List;
                _ -> get_data_sync(SyncAttempts-1)
            end;
        _ ->
            timer:sleep(100),
            get_data_sync(SyncAttempts-1)
    end.

%% ---------------------------------
%% return sorted registered names
%% ---------------------------------
registered_names() ->
    lists:usort(lists:map(fun({N,_}) -> N end, get_data())).

%% ---------------------------------
%% initiates full resync (by watch-dog)
%% ---------------------------------
resync(Nodes) ->
    gen_server:cast(?MODULE,{resync,Nodes}).

%% ---------------------------------
%% return all registered data
%% ---------------------------------
get_data_checksum() ->
    case catch ets:tab2list(?EtsNames) of
        List when is_list(List) -> {ok,erlang:phash2(List)};
        _ -> {error,ets}
    end.

%% ---------------------------------
%% sync heartbeat from worker node (where not server mode)
%% ---------------------------------
sync_heartbeat(_FromNode,_CheckSum) ->
    % @todo check if same data by from_node.
    %   NOTE! FromNode own data is less than reg server registered data.
    %   NOTE! ets:fold for every sync heartbeat is too high load, but delay is wrong, because data could be changed.
    ok.

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------------------------------------
%% Init
%% ------------------------------------------------------------
init(Opts) ->
    Names = ?BU:get_by_key(names,Opts),
    PidsEts = ets:new(reglib_pids,[set]),
    {Self,RefCfg,RefHeartbeat} = {self(),make_ref(),make_ref()},
    State = #state{mode=client,
                   ets_names=Names,
                   ets_pids=PidsEts,
                   lastupts=timestamp(),
                   cfgref=RefCfg,
                   cfgtimerref=erlang:send_after(?TIMEOUT_CFG,Self,{timer_cfg,RefCfg}),
                   % server timer
                   pingref=undefined, %RefPing,
                   pingtimerref=undefined, %erlang:send_after(?InitialPingTimeout,Self,{timer_ping,RefPing}),
                   % permanent timer
                   heartbeat_ref=RefHeartbeat,
                   heartbeat_timerref=erlang:send_after(?HeartbeatTimeout,Self,{timer_heartbeat,RefHeartbeat}),
                   pingclients_period_sec=max(0,?CFG:ping_clients_period_sec()),
                   pingclients_attempts_before_disconnect=max(1,?CFG:ping_clients_attempts_before_disconnect())},
    State1 = do_restore_monrefs(State),
    State2 = update_reg_server_nodes(State1),
    ?LOG('$info', "Reglib global_names inited (~120tp)", [Opts]),
    {ok, State2}.

%% ------------------------------------------------------------
%% Call
%% ------------------------------------------------------------

%% register name
handle_call({register_name,SenderType,Name,Pid}, _From, #state{mode=Mode}=State) ->
    #state{regserver_nodes=Nodes,ets_names=Names,ets_pids=PidsEts}=State,
    Fup = fun(NewPids) ->
                ?LOG('$info', "Reglib global_names. Reg: ~120tp -> ~120tp at '~ts'", [Name,Pid,node(Pid)]),
                MonRef = monitor_process(Pid),
                ets:insert(Names, {Name,NewPids}),
                ets:insert(PidsEts, {Pid, #val{name=Name,pid=Pid,monref=MonRef,ts=timestamp()}}),
                case SenderType of
                    'client' ->
                        send_notify(Mode,Name,Pid,'up'),
                        forward_to_reg_servers(Nodes,{register_name,Name,Pid});
                    'server' ->
                        send_notify(Mode,Name,Pid,'up'),
                        ok;
                    'cache' ->
                        send_notify(Mode,Name,Pid,'cache'),
                        ok
                end
          end,
    case ets:lookup(Names, Name) of
        [] -> Fup([Pid]);
        [{_,Pids}] ->
            case ordsets:is_element(Pid,Pids) of
                true -> ok;
                false -> Fup(ordsets:add_element(Pid,Pids))
            end end,
    {reply, yes, State#state{lastupts=timestamp()}};

%% unregister name (local pids)
handle_call({unregister_name,Name,local}, _From, #state{regserver_nodes=Nodes,ets_names=Names}=State) ->
    case ets:lookup(Names,Name) of
        [] -> {reply, ok, State};
        [{_,Pids}] ->
            MyNode = node(),
            DelPids = lists:foldr(fun(Pid,Acc) ->
                                        case node(Pid) of
                                            MyNode -> [Pid|Acc];
                                            _ -> Acc
                                        end end, [], Pids),
            forward_to_reg_servers(Nodes, {unregister_name,Name,{srv,DelPids}}),
            {reply, ok, del_pids({Name,Pids},DelPids,State)}
    end;

%% unregister name (all pids)
handle_call({unregister_name,Name,all}, _From, #state{regserver_nodes=Nodes,ets_names=Names}=State) ->
    case ets:lookup(Names,Name) of
        [] -> {reply, ok, State};
        [{_,Pids}] ->
            forward_to_reg_servers(Nodes, {unregister_name,Name,{srv,all}}),
            {reply, ok, del_pids({Name,Pids},Pids,State)}
    end;

%% unregister name (all), when forwarding from other rpci
handle_call({unregister_name,Name,{srv,all}}, _From, #state{mode=server,ets_names=Names}=State) ->
    case ets:lookup(Names,Name) of
        [] -> {reply, ok, State};
        [{_,Pids}] -> {reply, ok, del_pids({Name,Pids},Pids,State)}
    end;

%% unregister name (selected pids), when forwarding from other rpci
handle_call({unregister_name,Name,{srv,DelPids}}, _From, #state{mode=server,ets_names=Names}=State) ->
    case ets:lookup(Names,Name) of
        [] -> {reply, ok, State};
        [{_,Pids}] -> {reply, ok, del_pids({Name,Pids},DelPids,State)}
    end;

%% return data (names & pids) in sync mode
handle_call({get_data_sync}, From, State) ->
    spawn(fun() -> gen_server:reply(From,get_data(fun(_) -> error end)) end),
    {noreply,State};

%% other
handle_call(Msg, _From, State) ->
    ?LOG('$warning', "Reglib global_names unknown call: ~120tp", [Msg]),
    {noreply, State}.

%% ------------------------------------------------------------
%% Cast
%% ------------------------------------------------------------

handle_cast({restart}, State) ->
    throw(restart),
    {noreply, State};

handle_cast({test_disable}, State) ->
    State1 = do_disable_srv(State),
    {noreply, State1};

handle_cast({test_enable}, State) ->
    State1 = do_enable_srv(State),
    {noreply, State1};

%% resync selected nodes (even if not connected)
handle_cast({resync,Nodes}, State) ->
    State1 = start_sync(lists:delete(node(),Nodes), State),
    {noreply, State1};

%% fast nodedown event from regserver (avoid tcp timeout 60 sec on server down)
handle_cast({'nodedown_by_regserver',_,_,_}, #state{mode=server}=State) -> {noreply, State};
handle_cast({'nodedown_by_regserver',DownNode,SrvNode,AtTS}=Key, #state{mode=client,regserver_nodes=Servers,ext=Ext}=State) ->
    ?LOG('$trace',"Node '~s' notified 'nodedown_by_regserver' by '~s' at ~p.",[DownNode,SrvNode,AtTS]),
    State1 = case lists:member(DownNode,nodes(connected)) of
                 false -> State;
                 true ->
                     IsServerDown = lists:member(DownNode,Servers),
                     case maps:get(Key,Ext,undefined) of
                         undefined when IsServerDown ->
                             ?LOG('$info',"Node '~s' found connected while 'nodedown_by_regserver' by '~ts' at ~p. It's server, skip.",[DownNode,SrvNode,AtTS]),
                             State;
                         undefined ->
                             %?LOG('$info',"Node '~s' notified 'nodedown' by regserver. Disconnect/connect...",[Node]),
                             %?LOG('$info',"Disconnect node '~s': ~120p", [Node,catch net_kernel:disconnect(Node)]),
                             %spawn(fun() -> ?LOG('$info',"Connect node '~s': ~120p", [Node,net_kernel:connect_node(Node)]) end),
                             ?LOG('$info',"Node '~s' found connected while 'nodedown_by_regserver' by '~ts' at ~p. Check ping...",[DownNode,SrvNode,AtTS]),
                             % the main purpose to disconnect hang tcp connection after server found it broken and notified on it
                             spawn(fun() -> ping(DownNode,0,#{disconnect => true}) end),
                             TimerRef = erlang:send_after(3000, self(), {clear_ext,Key}),
                             State#state{ext=Ext#{Key => {?BU:timestamp(),TimerRef}}};
                         _ ->
                             State
                     end
             end,
    {noreply, State1};

%% other
handle_cast(Msg, State) ->
    ?LOG('$warning', "Reglib global_names unknown cast: ~120tp", [Msg]),
    {noreply, State}.

%% ------------------------------------------------------------
%% Info
%% ------------------------------------------------------------

% check reg servers changed
handle_info({timer_cfg,Ref},#state{cfgref=Ref}=State) ->
    State1 = update_reg_server_nodes(State),
    {Self,RefCfg} = {self(),make_ref()},
    State2 = State1#state{cfgref=RefCfg,
                          cfgtimerref=erlang:send_after(?TIMEOUT_CFG,Self,{timer_cfg,RefCfg})},
    {noreply, State2};

% ping reg servers
handle_info({timer_ping,Ref},#state{mode=server,pingref=Ref,ets_names=Names,heartbeat_count=Cnt}=State) ->
    {PingTimeout,PingEnabled} = ping_timeout(),
    State1 = case PingEnabled of
                 false when Cnt > 0 -> State;
                 _ ->
                     case ets:info(Names,size) of
                         0 when Cnt > 0 -> State;
                         _ -> ping_reg_server_nodes(State)
                     end
             end,
    {Self,RefPing} = {self(),make_ref()},
    State2 = State1#state{pingref=RefPing,
                          pingtimerref=erlang:send_after(PingTimeout,Self,{timer_ping,RefPing})},
    {noreply, State2};

%% heartbeats
handle_info({timer_heartbeat,Ref},#state{mode=Mode}=State) ->
    #state{heartbeat_ref=Ref,
           heartbeat_count=Cnt,
           pingclients_period_sec=PingClientsPeriod}=State,
    State2 =
        case Mode of
            client ->
                if ?RESYNC(Cnt) -> check_sync_heartbeat(State); % really dummy
                   true -> State
                end;
            server ->
                State1 = if ?RESYNC(Cnt) -> check_resync(State); % really dummy
                            true -> State
                         end,
                if (PingClientsPeriod > 0) andalso (Cnt rem PingClientsPeriod == 1) -> ping_clients_nodes(State);
                    true -> State1
                end
        end,
    Ref1 = make_ref(),
    State3 = State2#state{heartbeat_count=Cnt+1,
                          heartbeat_ref=Ref1,
                          heartbeat_timerref=erlang:send_after(?HeartbeatTimeout,self(),{timer_heartbeat,Ref1})},
    {noreply, State3};

%% DOWN of registered process
handle_info({'DOWN',MonRef,process,Pid,Reason}, #state{mode=Mode,ets_names=Names,ets_pids=PidsEts}=State) ->
    case ets:lookup(PidsEts,Pid) of
        [] -> {noreply,State};
        [{_,#val{name=Name,monref=MonRef}}] ->
            ets:delete(PidsEts, Pid),
            case ets:lookup(Names, Name) of
                [] -> {noreply,State};
                [{_,Pids}] ->
                    ?LOG('$info', "Reglib global_names. Down: ~120tp (~120tp at '~ts')~n\tReason: ~120tp", [Name,Pid,node(Pid),Reason]),
                    send_notify(Mode,Name,Pid,'down'),
                    case lists:delete(Pid,Pids) of
                        [] -> ets:delete(Names, Name);
                        Pids1 -> ets:insert(Names,{Name,Pids1})
                    end,
                    {noreply, State#state{lastupts=timestamp()}}
            end;
        [{_,Val}] ->
            ?LOG('$warning', "Reglib global_names. Down failure: pid=~p, monref=~p, found value=~120tp", [MonRef,Pid,Val]),
            {noreply, State}
    end;

%% if node disconnected
handle_info({'nodedown',DownNode,_Opts}, #state{mode=Mode}=State) ->
    ?LOG('$info', "Reglib global_names. nodedown '~ts'",[DownNode]),
    State2 = case Mode of
                 client -> ok;
                 server ->
                     #state{sitenodes=SiteNodes, regserver_nodes=ServerNodes}=State1 = refresh_site_nodes(State),

                     case lists:member(DownNode,ServerNodes) of
                         true -> State1;
                         false ->
                             CurNode = node(),
                             Nodes1 = lists:delete(CurNode,SiteNodes) -- ServerNodes,
                             lists:foreach(fun(N) -> gen_server:cast({?MODULE,N},{'nodedown_by_regserver',DownNode,CurNode,?BU:timestamp()}) end, Nodes1),
                             State1
                     end
             end,
    {noreply, State2};

%% if node connected
handle_info({'nodeup',Node,_Opts}, State) ->
    ?LOG('$info', "Reglib global_names. nodeup '~ts'",[Node]),
    ping(Node,0,#{disconnect => false}), % refresh ping result in storage before monitor registered pids.
    State1 = start_sync([Node],1000,State),
    {noreply, State1};

%% some internal clear ext key (ex. {'nodedown_by_regserver',Node,_,_})
handle_info({clear_ext,Key}, #state{ext=Ext}=State) ->
    {noreply, State#state{ext=maps:remove(Key,Ext)}};

%% if got sync error
handle_info({'error_resync',ErrNodes,RN,Ref}, State) ->
    Connected = nodes(connected),
    case ordsets:intersection(ordsets:from_list(Connected), ordsets:from_list(ErrNodes)) of
        [] -> {noreply,State};
        RetryNodes ->
            ?LOG('$info', "Reglib global_names. Retry(~tp) sync ~tp ~10tp",[RN,Ref,RetryNodes]),
            State1 = start_sync(RetryNodes,0,RN+1,State),
            {noreply, State1}
    end;

%% when got sync result
handle_info({'sync_result',Ref,Nodes,Res}, #state{syncs=Syncs}=State) ->
    State1 = case ordsets:is_element(Ref, Syncs) of
                 false -> State;
                 true -> apply_sync_result(Nodes,Ref,Res,State#state{syncs=ordsets:del_element(Ref, Syncs)})
             end,
    {noreply, State1};

%% when got ping all nodes async result
handle_info({pingclients_reply,Ref,Result}, #state{mode=server,pingclients_ref=Ref,pingclients_results=PingResults}=State) ->
    PingResults1 = lists:foldl(fun({Node,ok},Acc) -> maps:remove(Node,Acc);
                                  ({Node,_},Acc) -> Acc#{Node => maps:get(Node,Acc,0) + 1}
                               end, PingResults, Result),
    {noreply, ping_failed_clients_nodes(State#state{pingclients_results=PingResults1})};

%% Restart
handle_info({restart}, State) ->
    {stop, normal, State};

%% other
handle_info(Msg, State) ->
    ?LOG('$warning', "Reglib global_names unknown msg: ~120tp", [Msg]),
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% ----------------------------------------
%% @private
%%   restore monrefs to new current pid (when restart)
%% ----------------------------------------
do_restore_monrefs(#state{ets_pids=PidsEts,ets_names=Names}=State) ->
    ets:foldl(fun({_,#val{monref=MonRef}},_) -> demonitor_process(MonRef) end, ok, PidsEts),
    ets:delete_all_objects(PidsEts),
    NowTS = timestamp(),
    ets:foldl(fun({Name,Pids},_) ->
                        lists:foreach(fun(Pid) ->
                                            MonRef = monitor_process(Pid),
                                            ets:insert(PidsEts, {Pid,#val{name=Name,pid=Pid,monref=MonRef,ts=NowTS}})
                                      end, Pids)
              end, ok, Names),
    State.

%% ----------------------------------------
%% @private
%%   refresh configuration of reg server nodes.
%%   enable/disable storage of remote pids
%% ----------------------------------------
update_reg_server_nodes(#state{regserver_nodes=Nodes}=State) ->
    case ?CFG:get_server_nodes() of
        {ok,Nodes} -> State;
        {ok,Nodes1} ->
            State0 = State#state{regserver_nodes=Nodes1},
            case lists:member(node(),Nodes1) of
                true -> do_enable_srv(State0);
                false -> do_disable_srv(State0)
            end end.

%% ----------------------------------------
%% @private
%%   enable reg server (active mode, store local and remote pids)
%% ----------------------------------------
do_enable_srv(#state{mode=server}=State) -> State;
do_enable_srv(#state{mode=client}=State) ->
    ?LOG('$info', "Reglib global_names. Start server mode"),
    ok = net_kernel:monitor_nodes(true,[{node_type,all}]),
    Connected = nodes(connected),
    ?LOG('$trace', "Reglib global_names. . Connected: ~10p", [ordsets:from_list(Connected)]),
    State1 = start_sync(Connected, State),
    ping_reg_server_nodes(State1#state{mode=server}).

%% ----------------------------------------
%% @private
%   disable reg server (passive mode, store only local pids)
%% ----------------------------------------
do_disable_srv(#state{mode=client}=State) -> State;
do_disable_srv(#state{mode=server,ets_pids=PidsEts,ets_names=Names,pingtimerref=PTRef}=State) ->
    ?LOG('$info', "Reglib global_name. Stop server mode"),
    ok = net_kernel:monitor_nodes(false,[{node_type,all}]),
    MyNode = node(),
    Locals = ets:foldl(fun({Pid,#val{name=Name,monref=MonRef}},Acc) ->
                                case node(Pid) of
                                    MyNode -> [{Name,Pid}|Acc];
                                    _ ->
                                        demonitor_process(MonRef),
                                        ets:delete(PidsEts, Pid),
                                        Acc
                                end end, [], PidsEts),
    ets:delete_all_objects(Names),
    lists:foreach(fun({Name,Pid}) ->
                        case ets:lookup(Names,Name) of
                            [] -> ets:insert(Names, {Name,Pid});
                            [{_,Pids}] -> ets:insert(Names, {Name,ordsets:add_element(Pid, Pids)})
                        end end, Locals),
    ?BU:cancel_timer(PTRef),
    State#state{mode=client,
                pingref=undefined,
                pingtimerref=undefined,
                lastupts=timestamp()}.

%% ----------------------------------------
%% @private
%%   start sync connected nodes
%% ----------------------------------------
start_sync(Nodes,State) -> start_sync(Nodes,0,0,State).
start_sync(Nodes,Sleep,State) -> start_sync(Nodes,Sleep,0,State).
start_sync(Nodes,Sleep,RN,State) ->
    #state{sitenodes=SiteNodes}=State1 = refresh_site_nodes(State),
    case ordsets:intersection(ordsets:from_list(Nodes), SiteNodes) of
        Nodes -> SyncNodes = Nodes;
        SyncNodes -> ?LOG('$info', "Reglib global_names. Filter sync nodes ~n\t~120p ~n\tby site: ~120p ~n\t-> ~120p", [Nodes,SiteNodes,SyncNodes])
    end,
    start_sync_1(SyncNodes,Sleep,RN,State1).

%% @private
start_sync_1([],_,_,State) -> State;
start_sync_1(Nodes,Sleep,RN,#state{syncs=Syncs}=State) ->
    {Self,Ref} = {self(),make_ref()},
    ?LOG('$trace', "Reglib global_names. Sync start ~120tp ~10p", [Ref,Nodes]),
    F = fun() ->
            case Sleep of 0 -> ok; _ -> timer:sleep(Sleep) end,
            {M,F,A} = {?MODULE,get_data_sync,[]},
            Res = ?BLmulticall:call_direct(Nodes,{M,F,A},7000),
            % errors
            Ferr = fun({N,error}) -> ?LOG('$info', "Reglib global_names. Sync ~p '~ts' got error",[Ref,N]), {true,N};
                      ({N,undefined}) -> ?LOG('$info', "Reglib global_names. Sync ~p '~ts' undefined",[Ref,N]), {true,N};
                      (_) -> false
                   end,
            case lists:filtermap(Ferr, Res) of
                [] -> ok;
                ErrNodes when RN < 20 -> erlang:send_after(erlang:min(RN*1000,10000), Self, {error_resync,ErrNodes,RN,Ref});
                ErrNodes -> ?LOG('$trace', "Reglib global_names. Sync stop '~120tp'", [ErrNodes])
            end,
            % normal
            Fok = fun({_,[_|_]=R}) -> {true,R};
                     (_) -> false
                  end,
            Merged = case lists:filtermap(Fok, Res) of
                         [] -> [];
                         [ResX] -> ResX;
                         Res1 ->
                             Fjoin = fun({Name,Pids},AccMap) ->
                                            case maps:find(Name,AccMap) of
                                                error -> maps:put(Name,Pids,AccMap);
                                                {_,Pids1} -> maps:put(Name,ordsets:union(Pids,Pids1),AccMap)
                                            end end,
                             JMap = lists:foldl(fun(NodeRes,Acc) -> lists:foldl(Fjoin, Acc, NodeRes) end, maps:new(), Res1),
                             maps:to_list(JMap)
                     end,
            Self ! {sync_result,Ref,Nodes,Merged}
        end,
    spawn_link(F),
    State#state{syncs=ordsets:add_element(Ref,Syncs)}.

%% @private
%%   apply received sync result
apply_sync_result(Nodes,Ref,Res,#state{ets_names=Names,ets_pids=PidsEts}=State) ->
    NowTS = timestamp(),
    F = fun({Name,Pids},Acc) ->
                NewPids = case ets:lookup(Names,Name) of
                              [] ->
                                  ets:insert(Names,{Name,Pids}),
                                  Pids;
                              [{_,Pids1}] ->
                                  ets:insert(Names,{Name,ordsets:union(Pids1,Pids)}),
                                  ordsets:subtract(Pids,Pids1)
                          end,
                case NewPids of
                    [] -> Acc;
                    _ ->
                        ?LOG('$trace', "Reglib global_names. SyncReg: ~120tp -> ~120tp", [Name,NewPids]),
                        lists:foldl(fun(Pid,Acc1) ->
                                            MonRef = monitor_process(Pid),
                                            ets:insert(PidsEts, {Pid, #val{name=Name,pid=Pid,monref=MonRef,ts=NowTS}}),
                                            Acc1+1
                                    end, Acc, NewPids)
                end end,
    State1 = case lists:foldl(F,0,Res) of
                 0 -> State;
                 _ -> State#state{lastupts=timestamp()}
             end,
    ?LOG('$trace', "Reglib global_names. Sync done ~tp ~10p", [Ref,Nodes]),
    State1.

%% ----------------------------------------
%% @private
%% ----------------------------------------
monitor_process(Pid) when is_pid(Pid) ->
    % should not be PANG from storage not to skip when registering {node,Node}.
    % But storage is updated in nodeup.
    case ping(node(Pid),1000,#{disconnect => false}) of
        pong ->
            erlang:monitor(process,Pid);
        Pang ->
            ?LOG('$warning', "Reglib global_names. Monitor ~ts failed: ~120tp", [node(Pid),Pang]),
            MonRef = make_ref(),
            self() ! {'DOWN',MonRef,process,Pid,nodedown},
            MonRef
    end.

%% ----------------------------------------
%% @private
%% ----------------------------------------
demonitor_process(Ref) when is_reference(Ref) ->
    erlang:demonitor(Ref).

%% ----------------------------------------
%% @private
%% del all pids of name
%% ----------------------------------------
del_pids({Name,Pids},Pids,#state{ets_names=Names,ets_pids=PidsEts}=State) ->
    ?LOG('$trace', "Reglib global_names. Unreg: '~ts' -> ~120p", [Name,Pids]),
    ets:delete(Names, Name),
    lists:foreach(fun(Pid) ->
                        case ets:lookup(PidsEts,Pid) of
                            [] -> ok;
                            [{_,#val{monref=MonRef,name=Name}}] ->
                                ets:delete(PidsEts,Pid),
                                demonitor_process(MonRef)
                        end
                  end, Pids),
    State#state{lastupts=timestamp()};
%% del selected pids of name
del_pids({Name,Pids},DelPids,#state{ets_names=Names,ets_pids=PidsEts}=State) ->
    ?LOG('$trace', "Reglib global_names. Unreg: '~ts' -> ~120p", [Name,DelPids]),
    case ordsets:subtract(Pids,DelPids) of
        [] -> ets:delete(Names, Name);
        [_|_]=NewPids -> ets:insert(Names,{Name,NewPids})
    end,
    lists:foreach(fun(Pid) ->
                        case ets:lookup(PidsEts,Pid) of
                            [] -> ok;
                            [{_,#val{monref=MonRef,name=Name}}] ->
                                ets:delete(PidsEts,Pid),
                                demonitor_process(MonRef)
                        end
                  end, DelPids),
    State#state{lastupts=timestamp()}.

%% ----------------------------------------
%% @private
%% ----------------------------------------
forward_to_reg_servers(Nodes,{register_name,Name,Pid}) ->
    F = fun() ->
            Nodes1 = lists:delete(node(),Nodes),
            {M,F,A} = {?MODULE,fwd_register_name,[Name,Pid]},
            ?BLrpc:multicall(Nodes1, M, F, A, 5000)
        end,
    spawn(F);
forward_to_reg_servers(Nodes,{unregister_name,Name,ModeArg}) ->
    F = fun() ->
            Nodes1 = lists:delete(node(),Nodes),
            {M,F,A} = {?MODULE,fwd_unregister_name,[Name,ModeArg]},
            ?BLrpc:multicall(Nodes1, M, F, A, 5000)
        end,
    spawn(F).

%% ----------------------------------------
%% @private
%% ----------------------------------------
ping_reg_server_nodes(#state{mode=client}=State) -> State;
ping_reg_server_nodes(#state{mode=server,regserver_nodes=ServerNodes}=State) ->
    Nodes1 = lists:delete(node(),ServerNodes),
    lists:foreach(fun(N) -> spawn(fun() -> ping(N,1000,#{disconnect => true}) end) end, Nodes1), % intra-server ping no need to disconnect at all
    State.

%% @private
ping_clients_nodes(#state{mode=client}=State) -> State;
ping_clients_nodes(#state{mode=server,regserver_nodes=ServerNodes}=State) ->
    #state{sitenodes=SiteNodes}=State1 = refresh_site_nodes(State),
    Nodes1 = lists:delete(node(),SiteNodes) -- ServerNodes,
        Self = self(),
    Ref = make_ref(),
    spawn(fun() -> Self ! {pingclients_reply,Ref,spawned_ping_clients_nodes(Nodes1)} end),
    State1#state{pingclients_ref=Ref}.

%% @private
spawned_ping_clients_nodes(Nodes) ->
    % no need neither to ping nor to disconnect. Wait reply, count replies, wait for pang_to_disconnect_sec timeout.
    PingOpts = #{use_ping => false},
    Res = ?BLmulticall:call_direct(Nodes, {erlang, node, []}, 900, PingOpts),
    lists:map(fun({Node1,Node2}) when Node1==Node2 -> {Node1,ok}; (R) -> R end, Res).

%% @private
%% filter nodes that pang/timeout too long, ping/disconnect them and clear from pingclients_results.
%% Other nodes keep ing pingclients_results for further checks
ping_failed_clients_nodes(#state{mode=client}=State) -> State;
ping_failed_clients_nodes(#state{mode=server}=State) ->
    #state{pingclients_results=PingResults,
           pingclients_attempts_before_disconnect=MaxPangs}=State,
    ToPingDisconnect = lists:filtermap(fun({Node,N}) when N >= MaxPangs -> {true,Node}; (_) -> false end, maps:to_list(PingResults)),
    PingResults1 = maps:without(ToPingDisconnect,PingResults),
    % the main purpose - disconnect after few seconds after tcp hangs, and notify other nodes about nodedown
    lists:foreach(fun(N) -> spawn(fun() -> ping(N,0,#{disconnect => true}) end) end, ToPingDisconnect),
    State#state{pingclients_results=PingResults1}.

%% --------
%% @private
ping(N,_CacheTTLMs,Opts) ->
    %?BLping:ping(N,{1000,CacheTTLMs},Opts)
    ?PING:ping(N,1000,Opts). % timeout before failure

%% ----------------------------------------
%% @private
%  when Name is registered by more than 1 pid
%% ----------------------------------------
select_pid(_Name,Pids) ->
    F = fun(Pid,Acc) ->
                case ping(node(Pid),1000,#{disconnect => false}) of
                    pong -> throw(Pid);
                    _ -> Acc
                end end,
    try lists:foldl(F, undefined, Pids)
    catch _:R -> R
    end.

%% ----------------------------------------
%% @private
%%   periodical full check (if server mode)
%%   if registered data is the same on reg servers and contains all registered worker servers processes
%%   it's general watchdog
%% ----------------------------------------
check_resync(#state{mode=server}=State) -> State.

%% ----------------------------------------
%% @private
%%   periodical force heartbeat check (if not server mode)
%%   SKIP REALLY SENDING DATA TO REG SERVER, IT'S OVERHEAD.
%% ----------------------------------------
check_sync_heartbeat(State) -> State.
%% check_sync_heartbeat(#state{mode=client,regserver_nodes=Nodes,names=Names}=State) ->
%%     CS = erlang:phash2(lists:usort(ets:tab2list(Names))),
%%     F = fun() ->
%%             {M,F,A} = {?MODULE,sync_heartbeat,[node(),CS]},
%%             ?ENVMULTICALL:call_direct(Nodes,{M,F,A},5000)
%%         end,
%%     spawn(F),
%%     State.

%% ----------------------------------------
%% @private
%% ----------------------------------------
refresh_site_nodes(State) ->
    case ?CFG:get_sync_nodes() of
        {ok,Nodes} -> State#state{sitenodes=ordsets:from_list(Nodes)};
        _ -> order_restart(State)
    end.

%% @private
order_restart(State) ->
    ?LOG('$trace', "Reglib global_names. Configuration undefined. Restart ordered!"),
    erlang:send_after(1000,self(),{restart}),
    State.

%% ----------------------------------------
%% @todo check monitored nodes by ping (if server mode)
%% @todo logs & outs

timestamp() -> ?BU:timestamp().

%% @private
ping_timeout() ->
    case ?CFG:ping_timeout() of
        I when I =< 0 -> {10000,false};
        Timeout -> {Timeout,true}
    end.

%% @private
send_notify(server,Name,Pid,Status) -> ?CFG:notify(Name,Pid,Status);
send_notify(_,_,_,_) -> ok.