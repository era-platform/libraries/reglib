%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.05.2021
%%% @doc Server facade module. Used both on clients and servers.

-module(reglib_server).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([check_connection/0,
         whereis_name/1,
         whereis_name_all/1,
         get_nodes/1,
         registered_names/0,
         registered_pids/0]).

-export([set_group_leader/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------------
%% Check connection from client's cache server
%% ------------------------------------
check_connection() ->
    Res = {ok, node()},
    %?LOG('$trace', "Reglib. check_connection -> ~120tp", [Res]),
    Res.

%% ------------------------------------
%% Global process detection from client's module
%% ------------------------------------

%% general request to find pid by name
whereis_name(GlobalName) ->
    Res = ?NAME:whereis_name(GlobalName),
    %?LOG('$trace', "Reglib. whereis(~120tp) -> ~120tp", [GlobalName, Res]),
    Res.

%% general request to return all registered pids by name
whereis_name_all(GlobalName) ->
    Res = ?NAME:whereis_name_all(GlobalName),
    %?LOG('$trace', "Reglib. whereis(~120tp) -> ~120tp", [GlobalName, Res]),
    Res.

%% general request to return all registered pids by name
get_nodes(GlobalName) ->
    Res = ?NAME:get_nodes(GlobalName),
    %?LOG('$trace', "Reglib. get_nodes(~120tp) -> ~120tp", [GlobalName, Res]),
    Res.

%% return all names, registered locally
registered_names() ->
    ?NAME:registered_names().

%% return all pids, registered locally.
registered_pids() ->
    ?NAME:get_data().

%% ====================================================================
%% Internal functions
%% ====================================================================

% setup group leader for current proc, to redirect rpc input-output and avoid phantom connections
set_group_leader() ->
    group_leader(?STATE:get_group_leader(), self()).
