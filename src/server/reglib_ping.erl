%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2023 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 11.12.2023
%%% @doc Local ping without store. For rpc server usage only, where mass-ping is used from one case only.

-module(reglib_ping).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    ping/3,
    pong/0,
    ping_ok/1
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

% ping cached and async timeout

ping(Node, PingTimeout, Opts)
  when is_atom(Node), is_integer(PingTimeout), PingTimeout > 0 ->
    do_ping(Node, PingTimeout, Opts).

% pong
pong() -> pong.

% ping ok (to store and not to do new ping requests on wide flow of requests)
ping_ok(_Node) ->
    % dummy
    ok.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----
%% @private
do_ping(Node, PingTimeout, Opts) when is_atom(Node) ->
    {_,R} = timer:tc(fun() -> do_ping_rpc(Node, PingTimeout, Opts) end),
    R.

%% ping over spawned rpc to guarantee timeout on network failure after tcp is interrupted
do_ping_rpc(Node, PingTimeout, Opts) ->
    try erpc:call(Node, ?BLping, pong, [], 1000) of
        pong -> pong
    catch
        error:{erpc,noconnection} ->
            pang;
        error:{erpc,timeout} ->
            disconnect(Node, maps:get(disconnect,Opts)),
            timeout;
        error:{erpc,notsup} ->
            do_ping_rpc_reserve(Node,PingTimeout,Opts);
        _:_ ->
            pang
    end.

%% @private
do_ping_rpc_reserve(Node, PingTimeout, Opts) ->
    case ?BLrpc:call(Node, ?BLping, pong, [], PingTimeout) of
        {badrpc, timeout} ->
            disconnect(Node, maps:get(disconnect,Opts)),
            timeout;
        {_, _} ->
            pang;
        pong ->
            pong
    end.

%% @private
disconnect(Node, true) ->
    ?BLping:out('$info',"Node '~s' ping timeout (reglib). Disconnect/connect...",[Node]),
    ?BLping:out('$info',"Disconnect node '~s': ~120p", [Node,catch net_kernel:disconnect(Node)]),
    spawn(fun() -> ?OUT('$info',"Connect node '~s': ~120p", [Node,net_kernel:connect_node(Node)]) end);
disconnect(Node, _) ->
    ?BLping:out('$info',"Node '~s' ping timeout (reglib). Wait...",[Node]),
    ok.

