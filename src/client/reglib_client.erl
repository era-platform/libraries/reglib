%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% 04.05.2021
%%% @doc Client facade module. Used on client node.

-module(reglib_client).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([whereis_name/1,
         whereis_name_all/1,
         get_nodes/1,
         %
         register_name/2,
         unregister_name/1,
         unregister_name_locally/2,
         unregister_name_globally/2,
         send/2,
         %
         registered_names/0,
         registered_names_local/0,
         registered_names_local_size/0,
         %
         registered_pids/0,
         registered_pids_local/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ------------------------------------
%% Asks reg server for Pid of GlobalName.
%% ------------------------------------
whereis_name(GlobalName) ->
    %  first, attempt to find on current node (in cache)
    case whereis_name('local',GlobalName) of
        undefined ->
            % then check on server (if current node is not a server)
            whereis_name('server',GlobalName);
        Pid when is_pid(Pid) ->
            ?LOG('$trace', "Reglib. whereis(~120tp) local -> ~120tp", [GlobalName, Pid]),
            Pid
    end.

%% @private
whereis_name('local',GlobalName) ->
    ?NAME:whereis_name(GlobalName);

%% @private
whereis_name('server',GlobalName) ->
    % timeout 1000 ms, cause 60 sec over connection loss auto detected and hang off.
    case forward(whereis_name, [GlobalName], 1000) of
        {error,_}=Err ->
            ?LOG('$trace', "Reglib. whereis(~120tp) -> ~120tp", [GlobalName, Err]),
            undefined;
        undefined -> undefined;
        Pid when is_pid(Pid) ->
            cache_register_name(GlobalName,Pid),
            ?LOG('$trace', "Reglib. whereis(~120tp) -> ~120tp", [GlobalName, Pid]),
            Pid;
        Term ->
            ?LOG('$trace', "Reglib. whereis(~120tp) -> ~120tp", [GlobalName, Term]),
            Term
    end.

%% ------------------------------------
%% Asks reg server for Pid of GlobalName.
%% ------------------------------------
whereis_name_all(GlobalName) ->
    %  first, attempt to find on current node (in cache)
    case whereis_name_all('local',GlobalName) of
        [] ->
            % then check on server (if current node is not a server)
            whereis_name_all('server',GlobalName);
        Pids when is_list(Pids) ->
            ?LOG('$trace', "Reglib. whereis(~120tp) local -> ~120tp", [GlobalName, Pids]),
            Pids
    end.

%% @private
whereis_name_all('local',GlobalName) ->
    ?NAME:whereis_name_all(GlobalName);

%% @private
whereis_name_all('server',GlobalName) ->
    % timeout 1000 ms, cause 60 sec over connection loss auto detected and hang off.
    case forward(whereis_name_all, [GlobalName], 1000) of
        {error,_}=Err ->
            ?LOG('$trace', "Reglib. whereis(~120tp) -> ~120tp", [GlobalName, Err]),
            [];
        undefined -> [];
        Term ->
            ?LOG('$trace', "Reglib. whereis(~120tp) -> ~120tp", [GlobalName, Term]),
            Term
    end.

%% -------------------------------------
%% Asks reg server for nodes of GlobalName.
%% -------------------------------------
get_nodes(GlobalName) ->
    % timeout 1000 ms, cause 60 sec over connection loss auto detected and hang off.
    case forward(get_nodes, [GlobalName], 1000) of
        {error,_}=Err ->
            ?LOG('$trace', "Reglib. get_nodes(~120tp) -> ~120tp", [GlobalName, Err]),
            undefined;
        undefined -> undefined;
        Term ->
            ?LOG('$trace', "Reglib. get_nodes(~120tp) -> ~120tp", [GlobalName, Term]),
            Term
    end.

%% -------------------------------------
%% register name on regserver. By local name_srv
%% -------------------------------------
register_name(Name,Pid) ->
    ?NAME:register_name(Name,Pid).

cache_register_name(Name,Pid) ->
    ?NAME:cache_register_name(Name,Pid).

%% -------------------------------------
%% unregister name on regservers. By local name_srv
%% -------------------------------------
unregister_name(Name) ->
    ?NAME:unregister_name(Name).

unregister_name_globally(Name,ModeArg) ->
    ?NAME:unregister_name_globally(Name,ModeArg).

unregister_name_locally(Name,ModeArg) ->
    ?NAME:unregister_name_locally(Name,ModeArg).

%% -------------------------------------
%% send to named process
%% -------------------------------------
send(Name,Msg) ->
    F = fun(Pid) -> Pid ! Msg, Pid end,
    case whereis_name(Name) of
        undefined -> {badarg, {Name, Msg}};
        Pid when is_pid(Pid) -> F(Pid);
        [Pid] when is_pid(Pid) -> F(Pid);
        [_Pid|_]=Pids when is_pid(_Pid) ->
            F(lists:nth(?BU:random(length(Pids)) + 1, Pids));
        _ -> {badarg, {Name, Msg}}
    end.

%% -------------------------------------
%% return registered names from regserver
%%   union of local and regserver's items
%% -------------------------------------
registered_names() ->
    Loc = ?NAME:registered_names(),
    {ok,Nodes} = ?CFG:get_server_nodes(),
    case lists:member(node(), Nodes) of
        true -> Loc;
        false ->
            case forward(registered_names, [], 5000) of
                {error,_}=Err ->
                    ?LOG('$error', "Reglib. registered_names -> ~120tp", [Err]),
                    Loc;
                undefined -> Loc;
                Rem when is_list(Rem) -> ordsets:union(Loc,Rem)
            end end.

%% -------------------------------------
%% return registered names (only locals, without call to reg server)
%% -------------------------------------
registered_names_local() ->
    ?NAME:registered_names().

%% -------------------------------------
registered_names_local_size() ->
    ?NAME:get_size().

%% -------------------------------------
registered_pids() ->
    Loc = ?NAME:get_data(),
    {ok,Nodes} = ?CFG:get_server_nodes(),
    case lists:member(node(), Nodes) of
        true -> Loc;
        false ->
            case forward(registered_pids, [], 5000) of
                {error,_}=Err ->
                    ?LOG('$error', "Reglib. registered_names -> ~120tp", [Err]),
                    Loc;
                undefined -> Loc;
                Rem when is_list(Rem) -> ordsets:union(Loc,Rem)
            end end.

%% -------------------------------------
registered_pids_local() ->
    ?NAME:get_data().

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------
forward(Fun, Args, Timeout) ->
    ServerNodes = ?STATE:get_active_server_nodes(),
    log_before(ServerNodes, Fun, Args),
    forward(ServerNodes, ServerNodes, Fun, Args, Timeout).

%% -------------------------------------
forward([], ServerNodes, Fun, Args, _) ->
    log_failed(ServerNodes, Fun, Args),
    {error, "Server node inaccessible"};
forward([ServerNode|Rest], ServerNodes, Fun, Args, Timeout) ->
    case ?BLping:ping(ServerNode, {1000,100}, #{disconnect => false}) of
        pong -> forward_1(ServerNode, Rest, ServerNodes, Fun, Args, Timeout);
        _ ->
            ?STATE:set_invalid_node(ServerNode),
            forward(Rest, ServerNodes, Fun, Args, Timeout)
    end.

%% @private
forward_1(ServerNode, Rest, ServerNodes, Fun, Args, Timeout) ->
    case ?BLrpc:call(ServerNode, ?SRV, Fun, Args, Timeout+500) of
        {badrpc, _}=Err ->
            log_error(ServerNode, Err, Fun, Args),
            ?STATE:set_invalid_node(ServerNode),
            forward(Rest, ServerNodes, Fun, Args, Timeout);
        Term ->
            ?BLping:ping_ok(ServerNode),
            Term
    end.

%% ------------------------------
%% @private log
log_before(ServerNodes, Fun, [A1|_]=_Args) when Fun==whereis_name; Fun==whereis_name_all ->
    ?LOG('$trace', "Reglib client. ServerNodes: ~120p. Call: ~120p (~120p, ...)", [ServerNodes,Fun,A1]);
log_before(ServerNodes, Fun, _Args) ->
    ?LOG('$trace', "Reglib client. ServerNodes: ~120p. Call: ~120p", [ServerNodes,Fun]).
%log_before(_ServerNodes,_Fun,_Args) -> ok.

%% -------------------------------------
log_error(ServerNode, Err, Fun, [A1|_]=_Args) when Fun==whereis_name; Fun==whereis_name_all ->
    ?LOG('$warning', "Reglib client. Fwd error through ~120tp. Call: ~120tp (~120p, ...). ~n\tError: ~120p", [ServerNode,Fun,A1,Err]);
log_error(ServerNode, Err, Fun, _Args) ->
    ?LOG('$warning', "Reglib client. Fwd error through ~120tp. Call: ~120tp. ~n\tError: ~100p", [ServerNode,Fun,Err]).

%% -------------------------------------
log_failed(ServerNodes, Fun, [A1|_]=_Args) when Fun==whereis_name; Fun==whereis_name_all ->
    ?LOG('$info', "Reglib client. Fwd failed. ServerNodes: ~100p. Call: ~100p (~100p, ...)", [ServerNodes,Fun,A1]);
log_failed(ServerNodes, Fun, _Args) ->
    ?LOG('$info', "Reglib client. Fwd failed. ServerNodes: ~100p. Call: ~100p", [ServerNodes,Fun]).
