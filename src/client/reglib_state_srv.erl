%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @dt 04.05.2021
%%% @doc Genserver to store and refresh server nodes states

-module(reglib_state_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/0, stop/0,
         get_group_leader/0,
         get_active_server_nodes/0,
         set_invalid_node/1]).

-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).
-export([print/0]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(state, {
    group_leader_pid = undefined :: pid(),
    invalid_nodes = [],
    ping_ref :: reference(),
    ping_timerref :: reference()
}).

-define(TIMEOUT, 5000).

%% ====================================================================
%% API functions
%% ====================================================================

start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).

stop() ->
    gen_server:stop(?MODULE).

get_group_leader() ->
    gen_server:call(?MODULE, {get_group_leader}).

get_active_server_nodes() ->
    gen_server:call(?MODULE, {get_active_server_nodes}).

set_invalid_node(RpcNode) ->
    gen_server:cast(?MODULE, {set_invalid_node, RpcNode}).

print() ->
    gen_server:cast(?MODULE, {print}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------------------------------------
%% Init
%% ------------------------------------------------------------
init(Args) ->
    State = #state{group_leader_pid=group_leader()},
    ?LOG('$info', "Reglib state inited (~120tp)", [Args]),
    {ok, State}.

%% ------------------------------------------------------------
%% Call
%% ------------------------------------------------------------

%% pid of group_leader for current node
handle_call({get_group_leader}, _From, #state{group_leader_pid=Pid}=State) ->
      {reply, Pid, State};

%% request for rpc server from full-mesh cluster
handle_call({get_active_server_nodes}, _From, #state{invalid_nodes=[]}=State) ->
    {ok,Nodes} = ?CFG:get_server_nodes(),
    {reply, Nodes, State};
handle_call({get_active_server_nodes}, _From, #state{invalid_nodes=Invalid}=State) ->
    {ok,Nodes} = ?CFG:get_server_nodes(),
    Invalid1 = Invalid -- (Invalid -- Nodes),
    Reply = Nodes -- Invalid,
    {reply, Reply, State#state{invalid_nodes=Invalid1}};

%% print current state
handle_call({print}=Req, _From, #state{}=State) ->
    handle_cast(Req, State),
    {reply, ok, State};

%% restart
handle_call({restart}, _From, #state{}=State) ->
    ?LOG('$info', "Reglib. state restart requested", []),
    {stop, restart, ok, State};

%% other
handle_call(Msg, _From, State) ->
    ?LOG('$warning', "Reglib. state unknown call: ~120tp", [Msg]),
    {noreply, State}.

%% ------------------------------------------------------------
%% Cast
%% ------------------------------------------------------------

%% store inaccessable server node
handle_cast({set_invalid_node, Node}, #state{invalid_nodes=InvalidNodes, ping_timerref=TimerRef}=State) ->
    ?LOG('$info', "Reglib state. Set server node invalid: '~ts'", [Node]),
    State2 = case lists:member(Node, InvalidNodes) of
                 false ->
                     State1 = State#state{invalid_nodes = [Node|InvalidNodes]},
                     case TimerRef of
                         undefined ->
                             Ref1 = make_ref(),
                             State1#state{ping_ref=Ref1,
                                          ping_timerref = erlang:send_after(?TIMEOUT, self(), {try_ping_invalid,Ref1})};
                         _ -> State1
                     end;
                 true -> State
             end,
    {noreply, State2};

%% updates invalid nodes after ping
handle_cast({update_invalid_nodes, []}, #state{ping_timerref=undefined}=State) ->
    {noreply, State#state{invalid_nodes=[]}};
handle_cast({update_invalid_nodes, []}, #state{ping_timerref=TimerRef}=State) ->
    erlang:cancel_timer(TimerRef),
    {noreply, State#state{invalid_nodes=[],
                          ping_ref=undefined,
                          ping_timerref=undefined}};
handle_cast({update_invalid_nodes, InvalidNodes}, #state{ping_timerref=undefined}=State) ->
    Ref1 = make_ref(),
    State1 = State#state{invalid_nodes=InvalidNodes,
                         ping_ref=Ref1,
                         ping_timerref = erlang:send_after(?TIMEOUT, self(), {try_ping_invalid,Ref1})},
    {noreply, State1};
handle_cast({update_invalid_nodes, InvalidNodes}, #state{}=State) ->
    {noreply, State#state{invalid_nodes=InvalidNodes}};

%% print current state
handle_cast({print}, #state{invalid_nodes=Invalid, ping_timerref=TimerRef}=State) ->
    ?OUT('$force', "Reglib. state: invalid_nodes=~120p, timer_ref=~120p", [Invalid, TimerRef]),
    {reply, ok, State};

%% restart
handle_cast({restart}, #state{}=State) ->
    {stop, restart, State};

%% other
handle_cast(Msg, State) ->
    ?LOG('$warning', "Reglib. state unknown cast: ~120tp", [Msg]),
    {noreply, State}.

%% ------------------------------------------------------------
%% Info
%% ------------------------------------------------------------

%% Time to ping invalid nodes
handle_info({try_ping_invalid,Ref}, #state{invalid_nodes=[], ping_ref=Ref}=State) ->
    {noreply, State};
handle_info({try_ping_invalid,Ref}, #state{invalid_nodes=[], ping_ref=Ref, ping_timerref=TimerRef}=State) ->
    erlang:cancel_timer(TimerRef),
    {noreply, State#state{ping_ref=undefined,
                          ping_timerref=undefined}};
handle_info({try_ping_invalid,Ref}, #state{invalid_nodes=InvalidNodes, ping_ref=Ref}=State) ->
    spawn(fun() -> try_ping_invalid(InvalidNodes) end),
    {noreply, State#state{ping_ref=undefined,
                          ping_timerref=undefined}};

%% other
handle_info(Msg, State) ->
    ?LOG('$warning', "Reglib. state unknown msg: ~120tp", [Msg]),
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% spawned
try_ping_invalid(InvalidNodes) ->
    Nodes = try_ping_invalid(InvalidNodes, []),
    ?LOG('$trace', "Reglib state. Server nodes restored: ~120tp, invalid ~120tp", [InvalidNodes--Nodes, Nodes]),
    gen_server:cast(?MODULE, {update_invalid_nodes, Nodes}).

try_ping_invalid([], Invalid) -> lists:reverse(Invalid);
try_ping_invalid([Node|Rest], Invalid) ->
    % no need to disconnect from reg server, could wait unlimited
    case ?BLping:ping(Node, {1000,1000}, #{disconnect => false}) of
        timeout -> try_ping_invalid(Rest, [Node|Invalid]);
        pang -> try_ping_invalid(Rest, [Node|Invalid]);
        pong ->
            case ?BLrpc:call(Node, ?SRV, check_connection, [], 1000) of
                {badrpc, _} -> try_ping_invalid(Rest, [Node|Invalid]);
                {ok, Node} -> try_ping_invalid(Rest, Invalid)
            end end.
