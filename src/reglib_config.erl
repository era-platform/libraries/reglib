%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.05.2021
%%% @doc

-module(reglib_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    log_destination/1,
    get_server_nodes/0,
    get_sync_nodes/0,
    notify/3,
    ping_timeout/0,
    ping_clients_period_sec/0,
    ping_clients_attempts_before_disconnect/0
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% log_destination
log_destination(Default) ->
    ?BU:get_env(?APP, 'log_destination', Default).

%% get_server_nodes_function
get_server_nodes() ->
    case ?BU:get_env(?APP, 'get_server_nodes_function', undefined) of
        undefined -> {ok, []};
        F when is_function(F,0) -> F()
    end.

%% get_sync_nodes_function
get_sync_nodes() ->
    case ?BU:get_env(?APP, 'get_sync_nodes_function', undefined) of
        undefined -> {ok, []};
        F when is_function(F,0) -> F()
    end.

%% notify_function(Name,Pid,Status) -> ok
notify(Name,Pid,Status) ->
    case ?BU:get_env(?APP, 'notify_function', undefined) of
        undefined -> ok;
        F when is_function(F,3) -> spawn(fun() -> F(Name,Pid,Status) end)
    end.

%% ping_timeout - interval of timer for ping reg servers
ping_timeout() ->
    ?BU:to_int(?BU:get_env(?APP, 'ping_timeout', 1000), 1000).

%% ping_clients_period_sec
ping_clients_period_sec() ->
    ?BU:to_int(?BU:get_env(?APP, 'ping_clients_period_sec', 10), 10).

%% ping_clients_attempts_before_disconnect
ping_clients_attempts_before_disconnect() ->
    ?BU:to_int(?BU:get_env(?APP, 'ping_clients_attempts_before_disconnect', 1), 1).

%% ====================================================================
%% Internal functions
%% ====================================================================


